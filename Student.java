package TemaGit;

import java.util.List;

public class Student implements StudentInterface {

	public String id;
	public String nume;
	public String prenume;
	public String instructorId;
	public String numarTelefon;
	
	public Student(String id, String nume, String prenume, String instructorId, String numarTelefon) {
		super();
		this.id = id;
		this.nume = nume;
		this.prenume = prenume;
		this.instructorId = instructorId;
		this.numarTelefon = numarTelefon;
	}

	@Override
	public boolean verificareTelefon(String numarTelefon) {
		if(numarTelefon.length() == 13) {
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean verificareInstructorId(String instructorId, List<String> listaInstructoriId) {
		if(listaInstructoriId.contains(instructorId)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
}
