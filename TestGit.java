package TemaGit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestGit {

	public static void main(String[] args) {
		System.out.println("Hello Git! Denumirea proiectului de licenta este - Aplicatie multiplatforma pentru scoala de soferi - ");
		Student s1 = new Student("123","Vulpe","Ioana","321","0769742352");
		Student s2 = new Student("124","Vulpe","Monica","322","+400769742352");
		List<String> listaMea = Arrays.asList("321","234");
		
		verificareInstructor(s1,listaMea);
		verificareInstructor(s2,listaMea);
		verificareTelefon(s1);
		verificareTelefon(s2);
	}
	
	public static void verificareInstructor(Student s,List<String> myList) {
		if(s.verificareInstructorId(s.instructorId, myList)) {
			System.out.println("Studentul " + s.prenume + " " + s.nume + " are un instructorId valid!");
		}
		else {
			System.out.println("Studentul " + s.prenume + " " + s.nume + " are un instructorId invalid!");
		}
	}
	
	public static void verificareTelefon(Student s) {
		if(s.verificareTelefon(s.numarTelefon)) {
			System.out.println("Studentul " + s.prenume + " " + s.nume + " are un numar de telefon valid!");
		}
		else {
			System.out.println("Studentul " + s.prenume + " " + s.nume + " are un numar de telefon invalid!");
		}
	}

}
