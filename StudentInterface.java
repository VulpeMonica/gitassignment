package TemaGit;


import java.util.List;

public interface StudentInterface {
 public abstract boolean verificareTelefon(String numarTelefon);
 public abstract boolean verificareInstructorId(String instructorId, List<String> listaInstructoriId);
}
